import React from 'react';
import './App.css';

import json from './mock/form.json';
import { createRoot, registerMetaComponent } from 'breeze-leaf-core-fwk'
import { ComponentClass } from 'breeze-leaf-core-lib';
import { IComponent } from 'breeze-leaf-core-lib/src/interface/componentType';
import { Form, Input, Button, Checkbox } from 'antd';

const getJson = () => {
  registerMetaComponent(json.name, new ComponentClass(json as IComponent));
}

function App() {
  // const [value, setValue] = React.useState(null);
  getJson();
  const Comp = createRoot(json.name)
  return (
    <div className="App">
      {Comp}
      <hr />
      <div style={{ width: 400 }}>
        <Demo />
      </div>
    </div>
  );
}

export default App;



const Demo = () => {
  const onFinish = (values: any) => {
    console.log('Success:', values);
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };

  return (
    <Form
      name="basic_antd"
      labelCol={{ span: 8 }}
      wrapperCol={{ span: 16 }}
      initialValues={{ remember: true }}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      autoComplete="off"
    >
      <Form.Item
        label="Username"
        name="username"
        rules={[{ required: true, message: 'Please input your username!' }]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        label="Password"
        name="password"
        rules={[{ required: true, message: 'Please input your password!' }]}
      >
        <Input.Password />
      </Form.Item>

      <Form.Item name="remember" valuePropName="checked" wrapperCol={{ offset: 8, span: 16 }}>
        <Checkbox>Remember me</Checkbox>
      </Form.Item>

      <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
        <Button type="primary" htmlType="submit">
          Submit
        </Button>
      </Form.Item>
    </Form>
  );
};