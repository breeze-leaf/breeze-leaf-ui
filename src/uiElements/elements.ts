import * as antd from 'antd';
import { registerComponents, registerComponentProps, ServiceActionType, MetaManager, getStore } from 'breeze-leaf-core-fwk';
import { ElementClass } from 'breeze-leaf-core-lib';
export * from 'antd';

export const setComponentProp = (name: string) => {

  switch (name) {
    case 'Input':
    case 'AutoComplete':
    case 'Cascader':
    case 'Checkbox':
    case 'DatePicker':
    case 'Mentions':
    case 'Password':
    case 'RadioGroup':
    case 'Rate':
    case 'Select':
    case 'Search':
    case 'Slider':
    case 'Switch':
    case 'TextArea':
    case 'TimePicker':
    case 'TimeRangePicker':
    case 'Transfer':
    case 'TreeSelect':
    case 'Upload':
      registerComponentProps({
        [name]: (meta: ElementClass) => {
          const metaId = new MetaManager().getMetaName();
          const store = getStore(metaId);
          return {
            onChange: (value: any) => store.dispatch({
              type: ServiceActionType.StoreMutation,
              who: {
                name: meta.field || meta.uuid,
                uuid: meta.uuid
              },
              which: {
                field: `$root.${meta.field}`
              },
              payload: (name === 'Input'
                || name === 'RadioGroup'
                || name === 'Password'
                || name === 'TextArea'
                || name === 'Search'
              )
                ? value.target.value
                : (name === 'Checkbox'
                  ? value.target.checked
                  : value)
            })
          }
        }
      })
      break;

    case "Button":
      registerComponentProps({
        [name]: (meta: ElementClass) => {
          return {
            onClick: () => {
              const metaId = new MetaManager().getMetaName();
              const state = getStore(metaId);
              console.log('root state: ', state.getState());
            }
          }
        }
      })
      break;
  }
}

const components = Object.keys(antd).reduce((elements, element) => {
  // elements[element] = createElementWrapper(element, antd[element]);
  elements[element] = antd[element];
  setComponentProp(element);
  return elements;
}, {}) as { [key: string]: any }[];

registerComponents(components);