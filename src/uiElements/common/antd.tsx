import { TreeSelect, TimePicker, Select, Radio, Input, DatePicker, Checkbox, Form } from 'antd';

const { TreeNode } = TreeSelect;
const { RangePicker: TimeRangePicker } = TimePicker;
const { OptGroup } = Select;
const { Group: RadioGroup, Button: RadioButton } = Radio;
const { Password, Search, Group: InputGroup, TextArea } = Input;
const { RangePicker } = DatePicker;
const { Group: CheckboxGroup } = Checkbox;
const { Item: FormItem, List: FormList, ErrorList: FormErrorList, Provider: FormProvider } = Form;

export {
  TreeNode,
  RangePicker,
  OptGroup,
  RadioGroup,
  RadioButton,
  Password,
  Search,
  InputGroup,
  TextArea,
  TimeRangePicker,
  CheckboxGroup,
  FormItem,
  FormList,
  FormErrorList,
  FormProvider
}