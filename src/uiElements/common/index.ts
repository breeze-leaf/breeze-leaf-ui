import { registerComponents } from 'breeze-leaf-core-fwk';
import Text from './Text';
import * as antd from './antd';
import { setComponentProp } from '..';

export * from './Text';

const commons = { ...antd, Text };

const components = Object.keys(commons).reduce((elements, element) => {
  // elements[element] = createElementWrapper(element, commons[element]);
  elements[element] = commons[element];
  setComponentProp(element);
  return elements;
}, {}) as { [key: string]: any }[];

registerComponents(components);